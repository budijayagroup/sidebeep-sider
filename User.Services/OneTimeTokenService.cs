﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using User.Data;
using User.Repositories;

namespace User.Services
{
    public interface IOneTimeTokenService
    {
        OneTimeToken CreateOneTimeToken(OneTimeToken entity);
        bool UpdateOneTimeToken(OneTimeToken entity);
        bool DeleteOneTimeToken(OneTimeToken entity);
    }

    public class OneTimeTokenService : IOneTimeTokenService
    {
        private IOneTimeTokenRepository _oneTimeTokenRepository;

        public OneTimeTokenService(
            IOneTimeTokenRepository oneTimeTokenRepository)
        {
            _oneTimeTokenRepository = oneTimeTokenRepository;
        }

        public OneTimeToken CreateOneTimeToken(OneTimeToken entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _oneTimeTokenRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateOneTimeToken(OneTimeToken entity)
        {
            return _oneTimeTokenRepository.Update(entity);
        }

        public bool DeleteOneTimeToken(OneTimeToken entity)
        {
            return _oneTimeTokenRepository.Delete(entity);
        }
    }
}
